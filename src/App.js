import { useEffect, useState } from "react";
import "./App.css";

function App() {

  const [msg, setMsg] = useState(null);
  // const textArea = document.querySelector('.textarea');

  useEffect(() => {
    const data = window.localStorage.getItem("msg");
  })

  useEffect(() => {
    window.localStorage.setItem('msg', JSON.stringify(msg));
  }, [msg]);
  let data = localStorage.getItem('msg');
  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea className="textarea is-large" placeholder="Notes..."  value={(JSON.stringify(data))} onChange={((e) => setMsg(e.target.value))} />
          </div>
        </div>
        <button onClick={((e) => {
            window.localStorage.setItem('msg', msg);
        })} className="button is-large is-primary is-active">Save</button>
        <button onClick={((e) => 
            window.localStorage.removeItem('msg')
          )} className="button is-large">Clear</button>
      </div>
    </div>
  );
}

export default App;
